# -*- coding:utf-8 -*-
from fala_dialler.api import FalaDialler
from fala_dialler.queue import QueueAPI
from fala_dialler.customer import CustomerAPI, PhoneAPI
from fala_dialler.agent import AgentAPI


DOMAIN = 'teste.com:8000'
CLIENT_ID = 'efjB0xJoMJbmHnEfskNdT5zQrLu1ZbTYBjpTJOOK'
SECRET_ID =  'fOt2WVl3MVSKR3TZ5HIhSH5poFUwPlqS9vrEuxS6unh4REaLNuUyeGhPAXTKdxfGiXmLkuWsXf8xM89ktIQvlskLlCbZWm5VZYY5Pbt7M0Ch1HBFDolMmZj9nRR3cUFO'

api = FalaDialler(DOMAIN, CLIENT_ID, SECRET_ID)

def create_customers_test():
    queue = api.retrieve(1, QueueAPI)
    customers = []

    for index in range(1000):
        cd = CustomerAPI(index, 'Aron', '1112231231')
        p = PhoneAPI(index, '031', '991671664')
        cd.add_phone(p)
        customers.append(cd)

    queue.send_customers(customers)


def create_agent():
    # create
    agent = AgentAPI(30, 'aron', 432, '1234')
    api.create(agent)

    # retrieve
    agent = api.retrieve(agent.id, AgentAPI)
    print(agent)

    # update
    agent.name = "Tata"
    api.update(agent)

create_agent()
create_customers_test()