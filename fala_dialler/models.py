# -*- coding: utf-8 -*-

try:
    from django.db import models


    class AuthorizationDialler(models.Model):

        domain = models.CharField(
            verbose_name="Domínio",
            max_length=255
        )

        client_id = models.CharField(
            verbose_name="Client ID",
            max_length=50
        )

        secret_id = models.CharField(
            verbose_name="Secret ID",
            max_length=255
        )

        access_token = models.CharField(
            verbose_name="Token",
            max_length=50,
            blank=True,
            null=True
        )

        active = models.BooleanField(
            verbose_name="Ativo",
            default=True
        )

        class Meta:
            verbose_name = "Autorização discador"
            ordering = ['-pk']

except:
    print("FalaDialler: django não instalado")
