# -*- coding: utf-8 -*-

from .base import BaseAPI


class AgentAPI(BaseAPI):
    CREATE = "/api/agent/"
    UPDATE = "/api/agent/{}/"
    RETRIEVE = UPDATE

    def __init__(self,
                 id_extern,
                 name,
                 ramal,
                 password, id=None, api=None):
        self.id_extern = id_extern
        self.name = name
        self.ramal = ramal
        self.password = password
        self.id = id
        self.api = api
