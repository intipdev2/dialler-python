# -*- coding: utf-8 -*-


def get_state_display(clss, constant):
    for key, display in clss.CHOICES:
        if key == constant:
            return display


class QueueState(object):
    IDLE = "0"
    RUNNING = "1"
    FINALIZED = "2"

    CHOICES = (
        (IDLE, "Parado"),
        (RUNNING, "Rodando"),
        (FINALIZED, "Finalizado")
    )


class QueueItemState(object):
    WAITING = "0"
    CALLING = "1"
    IN_CALL = "2"
    FINALIZED = "3"

    CHOICES = (
        (WAITING, "Aguardando"),
        (CALLING, "Chamando"),
        (IN_CALL, "Em chamada"),
        (FINALIZED, "Finalizado"),
    )
    
    CHOICES_EDIT = (
        (WAITING, "Aguardando"),
        (FINALIZED, "Finalizado")
    )


class CallState(object):
    WAITING = "0"
    CALLING = "1"
    IN_CALL = "2"
    TERMINATE = "3"
    FINALIZED_SUCCESS = "4"
    FINALIZED_NO_ANSWER = "5"
    FINALIZED_NO_AGENT = "6"
    ERROR_GATEWAY = "7"
    ERROR_GATEWAY_NO_DISPONIBLE = "8"

    CHOICES = (
        (WAITING, "Esperando"),
        (CALLING, "Ligando"),
        (IN_CALL, "Em chamada"),
        (TERMINATE, "Terminado."),
        (FINALIZED_SUCCESS, "Finalizado(Com atendimento)"),
        (FINALIZED_NO_ANSWER, "Finalizado(Não atendida)"),
        (FINALIZED_NO_AGENT, "Finalizado(Sem agente disponivel"),
        (ERROR_GATEWAY, "Error do gateway"),
        (ERROR_GATEWAY_NO_DISPONIBLE, "Gateway não disponível!")
    )