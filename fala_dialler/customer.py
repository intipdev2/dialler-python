# -*- coding: utf-8 -*-

import re

from .base import BaseAPI


class CustomerAPI(BaseAPI):
    __slot__ = ['id_extern', 'name', 'cpf_or_cnpj', 'phones', 'api']

    def __init__(self, id_extern, name, cpf_or_cnpj, api=None):
        self.id_extern = id_extern
        self.cpf_or_cnpj = self.format_cpf_or_cnpj(cpf_or_cnpj)
        self.name = name
        self.api = api
        self.phones = []

    def format_cpf_or_cnpj(self, value):
        return "".join(re.findall(r'\d', str(value)))[:13]

    def has_phone(self):
        return len(self.phones)

    def add_phone(self, phone):
        if type(phone) is PhoneAPI:
            if phone not in self.phones:
                if phone.is_valid():
                    self.phones.append(phone)
                else:
                    return {"erro": "Telefone inválido!"}
        else:
            raise Exception("add_phone é necessário um objeto 'Phone' !")

    def get_phones(self):
        return [phone.serialize() for phone in self.phones]

    def is_valid(self):
        has_name = bool(self.name)
        has_cnpj_or_cnpj = bool(self.cpf_or_cnpj)
        has_id_extern = bool(self.id_extern)
        has_phones = bool(self.phones)
        return has_name and has_cnpj_or_cnpj and has_id_extern and has_phones

    def serialize(self):
        return {
            "id_extern": self.id_extern,
            "cpf_or_cnpj": self.cpf_or_cnpj,
            "name": self.name,
            "phones": self.get_phones()
        }


class PhoneAPI(BaseAPI):

    def __init__(self, id_extern, ddd, number, api=None):
        self.id_extern = id_extern
        self.ddd = self.format_ddd(ddd)
        self.number = self.format_number(number)
        self.api = api

    def format_number(self, number):
        return "".join(re.findall(r'\d', str(number)))

    def format_ddd(self, ddd):
        ddd = "".join(re.findall(r'\d', str(ddd)))
        return ddd[-2:]

    def is_valid(self):
        has_ddd = bool(self.ddd) and len(self.ddd) == 2
        has_number = bool(self.number) and len(self.number)
        return has_ddd and has_number
