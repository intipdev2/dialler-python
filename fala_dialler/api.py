# -*- coding: utf-8 -*-

import json
import requests

from .utils import save_token, get_token


class FalaDialler(object):
    AUTHORIZE_URL = "/o/token/"

    def __init__(self, domain, client_id=None, secret_id=None):
        self.domain = domain
        self.client_id = client_id
        self.secret_id = secret_id
        self.token = None

    def get_url(self, path):
        domain = self.domain.strip()

        if domain.startswith("http://"):
            domain = self.domain.replace("http://", "")
        
        if domain.endswith("/"):
            domain = domain[:len(domain) -1]

        return "http://%s%s" % (domain, path)

    def generate_token(self, client_id, secret_id):
        return self.authorize(client_id, secret_id)

    def get_token(self):
        if not self.token:
            if get_token:
                self.token = get_token(self.domain)

                if not self.token:
                    self.token = self.generate_token(
                        self.client_id, self.secret_id)
            else:
                self.token = self.generate_token(
                    self.client_id, self.secret_id)
            print("Token: %s" % self.token)

        return self.token

    def get_header(self):
        return {
            "Content-Type": "application/json",
            "Authorization": "Bearer %s" % self.get_token()
        }

    def authorize(self, client_id, secret_id):
        response = requests.post(
            self.get_url(self.AUTHORIZE_URL),
            auth=(client_id, secret_id),
            data={"grant_type": "client_credentials"})

        if response.status_code == 200:
            data = response.json()
            access_token = data.get("access_token")
            if save_token:
                save_token(self.domain, access_token)
            return access_token
        else:
            raise Exception(
                "Não foi possivel autenticar na api fala dialler")
        
        return self.token

    def make_request(self, path, data=None, method='get'):
        method = method.lower()
        func_method = getattr(requests, method, requests.get)
        if func_method:
            kwargs = {"headers": self.get_header()}
            if data:
                kwargs["data"] = data if method == 'get' else json.dumps(data)
            return func_method(self.get_url(path), **kwargs)

    def create(self, model):
        response = self.make_request(model.CREATE, model.serialize(), 'post')

        if response.status_code == 201:
            model.__dict__.update(response.json())
            model.api = self
            return model
        else:
            raise Exception(response.text)

    def retrieve(self, _id, model):
        response = self.make_request(model.RETRIEVE.format(_id))
        if response.status_code == 200:
            return model.parse(api=self, data=response.json())

    def update(self, model):
        response = self.make_request(
            model.UPDATE.format(model.id_extern), model.serialize(), 'put')

        if response.status_code == 200:
            model.__dict__.update(response.json())
            model.api = self
        else:
            raise Exception(response.text)

    def delete(self, clss, ids=[]):
        response = self.make_request(
            clss.DELETE.format(*ids), None, 'delete')

        if response.status_code == 204:
            return True
        else:
            raise Exception(response.text)
