# -*- coding: utf-8 -*-

from .base import BaseAPI
from .utils import chunks

QUEUE_CUSTOMER_BY_REQUEST = 100


class QueueAPI(BaseAPI):
    CREATE = "/api/queue/"
    LIST = CREATE
    RETRIEVE = "/api/queue/{}/"
    UPDATE = RETRIEVE
    DELETE_CUSTOMER = "/api/queue/{}/customer/{}/delete/"
    DELETE_ALL_CUSTOMER = "/api/queue/{}/customer/delete/"
    UPDATE_STATE = "/api/queue/{}/state/"
    CREATE_CUSTOMERS = "/api/queue/{}/customer/"

    def __init__(self, id_extern, name, state=None, id=None, api=None):
        self.id_extern = id_extern
        self.name = name
        self.id = id
        self.api = api
        self.state = state

    @classmethod
    def get_list(cls, api):
        data = api.make_request(cls.LIST, None)

        if data:
            return [cls.parse(item) for item in data]

    def serialize(self):
        return {
            "name": self.name,
            "id_extern": self.id_extern
        }

    def update_state(self, state):
        api = self.get_api()
        url = self.UPDATE_STATE.format(self.id)
        return api.make_request(url, {'state': state}, 'put')

    def delete_customer(self, customer_id):
        api = self.get_api()
        url = self.DELETE_CUSTOMER.format(self.id_extern, customer_id)
        response = api.make_request(url, None, 'delete')

    def delete_all_customers(self):
        api = self.get_api()
        url = self.DELETE_ALL_CUSTOMER.format(self.id_extern)
        response = api.make_request(url, None, 'delete')

    def send_customers(self, customers=[]):
        api = self.get_api()
    
        response_data = []
        url = self.CREATE_CUSTOMERS.format(self.id_extern)
        data_chunks = chunks(customers, QUEUE_CUSTOMER_BY_REQUEST)

        for ch_data in data_chunks:
            response = api.make_request(
                url, [ch.serialize() for ch in ch_data], 'post')
            if response:
                response_data.extend(customers)

        return response_data
