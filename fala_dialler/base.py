# -*- coding: utf-8 -*-


class BaseAPI(object):

    @classmethod
    def parse(cls, api, data):
        return cls(api=api, **data)

    def serialize(self):
        data = dict(self.__dict__)
        if "api" in data:
            data.pop("api")
        return data

    def get_api(self):
        if not self.api:
            raise Exception(
                "Fala dialler não está no escodo da classe.")
        
        return self.api
