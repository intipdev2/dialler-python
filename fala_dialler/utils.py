# -*- coding: utf-8 -*-

save_token = None
get_token = None


def chunks(l, n):
    """Yield successive n-sized chunks from l."""
    for i in range(0, len(l), n):
        yield l[i:i + n]


def django_get_queryset_count(queryset, count):
    index = 0
    lcount = queryset.count()
    while True:
        yield list(queryset[index:lcount + index])
        index += count

        if count <= index:
            break


try:
    from .models import AuthorizationDialler

    def get(domain):
        queryset = AuthorizationDialler\
            .objects.filter(domain=domain, active=True)
        authorization = queryset.first()

        if authorization:
            return authorization.access_token


    def save(domain, access_token):
        queryset = AuthorizationDialler.objects\
            .filter(domain=domain)
        authorization = queryset.first()

        if authorization:
            authorization.access_token = access_token
            authorization.active = True
            authorization.save(update_fields=['active', 'access_token'])
            return authorization

    get_token = get
    save_token = save
except:
    print("FalaDialler: django não instalado")
