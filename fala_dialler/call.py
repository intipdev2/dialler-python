# -*- coding: utf-8 -*-

from .base import BaseAPI

class CallAPI(BaseAPI):
    RETRIEVE = "/api/call/{}/"
    FILTER = "/api/call/"
    RESULT = "/api/results/"

    def __init__(self,
                 pk,
                 customer_extern,
                 phone_extern,
                 state,
                 record=None,
                 initialized_at=None,
                 finalized_at=None,
                 queue_extern=None,
                 api=None, **kwargs):
        self.id = pk
        self.state = state
        self.record = record
        self.customer_extern = customer_extern
        self.phone_extern = phone_extern
        self.queue_extern = queue_extern
        self.initialized_at = initialized_at
        self.finalized_at = finalized_at
        self.api = api
    
    @property
    def customer(self):
        return self.customer_extern
    
    @property
    def phone(self):
        return self.phone_extern

    @property
    def queue(self):
        return self.queue_extern

    @classmethod
    def filter(cls, api, **data):
        response = api.make_request(cls.FILTER, data=data)

        if response:
            return [Call(**d) for d in response.results]

        return []

    @classmethod
    def get_results(cls, api, **data):
        response = api.make_request(cls.RESULT, data=data)

        if response.status_code == 200:
            data = response.json()

            if response:
                return [cls.parse(api, d) for d in data]

        return []
